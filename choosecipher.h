#ifndef CHOOSECIPHER
#define CHOOSECIPHER

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QTextEdit>
#include <QRadioButton>
#include "chat.h"

class Choosecipher: public QWidget
{
  Q_OBJECT

  public:
    explicit Choosecipher(QWidget *parent = 0);
    void setAddress(QString address);
    void setPort(int port);
    void setUsername(QString username);

  private slots:
    void onEnter();
    void onCaesar();
    void onReverseCaesar();


  private:
    Chat         chatWindow;
    QLabel       *label;
    QRadioButton *caesarCipher;
    QRadioButton *reverseCipher;
    QTextEdit    *caesarText;
    QTextEdit    *reverseText;
    QPushButton  *enter;

    QString      address;
    int          port;
    QString      username;
    int          cipher;
    int          shift;

};

#endif // CHOOSECIPHER
