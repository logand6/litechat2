#ifndef CHAT_H
#define CHAT_H

#include <QWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QLabel>
#include <client.h>
#include "caesar.h"
#include <QList>
#include <QString>

class Chat : public QWidget
{
    Q_OBJECT
    public:
        Chat(QWidget *parent = 0);
        void setAddress(QString address);
        void setPort(int port);
        void setUsername(QString username);
        void onlineList(QString username);

        void setCipher(int cipher);
        void setShift(int shift);
        Client *client;

    public slots:
        void onSend();
    private slots:
        void readMessages();


    private:

        QTextEdit   *chatArea;
        QTextEdit   *message;
        QPushButton *sendButton;
        QLabel      *usersOnline;
        QTextEdit   *users;

        QString     address;
        int         port;
        QString     username;
        int         cipher;
        int         shift;
        Caesar      *caesar;


};

#endif // CHAT_H
