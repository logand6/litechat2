#ifndef SERVER_H
#define SERVER_H

#include <QStringList>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>
#include <QSet>


class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    ~Server();

signals:

public slots:

private slots:
    void readyRead();
    void disconnected();

protected:
    void incomingConnection(int socketfd);
private:
    QList<QTcpSocket*> clients;
    QList<QString> users;
};


#endif // SERVER_H
