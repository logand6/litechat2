#include "server.h"
#include <QTcpSocket>

Server::Server(QObject *parent) : QTcpServer(parent)
{

}

void Server::incomingConnection(int socketfd){
    QTcpSocket *client = new QTcpSocket(this);
    client->setSocketDescriptor(socketfd);
    clients.append(client);
    qDebug() << "New client from:" << client->peerAddress().toString();

    connect(client, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void Server::readyRead()
{
    QTcpSocket *client = (QTcpSocket*)sender();
    while(client->canReadLine())
    {
        QString line = client->readLine().trimmed();
        qDebug() << "Message from client:" << line;
        // write message to other clients
        foreach(QTcpSocket *otherClient, clients){
            otherClient->write(QString(line + "\n").toUtf8());
            qDebug() << "Clients: " << otherClient;
           }

    }
}

void Server::disconnected()
{
    QTcpSocket *client = (QTcpSocket*)sender();
    qDebug() << "Client disconnected:" << client->peerAddress().toString();


}

Server::~Server()
{

}

