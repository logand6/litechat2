#include "server.h"
#include <QCoreApplication>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    Server *server = new Server();
    bool success = server->listen(QHostAddress::Any, 6789);
    if(!success)
    {
        qFatal("Could not listen on port 6789");
    }
    qDebug() << "Server Started";
    return app.exec();
}
