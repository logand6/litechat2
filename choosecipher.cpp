#include "choosecipher.h"

Choosecipher::Choosecipher(QWidget *parent)
    : QWidget(parent)
{
  //0 for caesar and 1 for date
  cipher = 0;

  chatWindow.setFixedSize(525, 300);
  chatWindow.setWindowTitle("Chat");
  chatWindow.setAddress(this->address);
  chatWindow.setPort(this->port);
  chatWindow.setUsername(this->username);

  label = new QLabel("Please select a type of cipher and key", this);
  label->setGeometry(125, 20, 300, 30);

  caesarCipher = new QRadioButton("Caesar Cipher",this);
  caesarCipher->setGeometry(75, 60, 130, 30);

  caesarText = new QTextEdit(this);
  caesarText->setText("");
  caesarText->setGeometry(220, 60, 100, 30);

  reverseCipher = new QRadioButton("Reverse Caesar",this);
  reverseCipher->setGeometry(75, 100, 130, 30);

  reverseText = new QTextEdit(this);
  reverseText->setText("");
  reverseText->setGeometry(220, 100, 100, 30);

  enter = new QPushButton("Enter Chatroom", this);
  enter->setGeometry(175, 150, 150, 30);

  connect(enter, SIGNAL(clicked()), this, SLOT(onEnter()));
  connect(caesarCipher, SIGNAL(clicked(bool)), this, SLOT(onCaesar()));
  connect(reverseCipher, SIGNAL(clicked(bool)), this, SLOT(onReverseCaesar()));
}

void Choosecipher::setAddress(QString address){
    this->address = address;
}
void Choosecipher::setPort(int port){
    this->port = port;
}
void Choosecipher::setUsername(QString username){
    this->username = username;
}

void Choosecipher::onEnter()
{
    // 0 caesar, 1 date
    if(cipher == 0){
        chatWindow.setCipher(0);
        chatWindow.setShift(caesarText->toPlainText().toInt());
    }
    else{
        chatWindow.setCipher(1);
        chatWindow.setShift(reverseText->toPlainText().toInt());
    }
    if(shift < 0){
        return;
    }

    chatWindow.setUsername(this->username);
    chatWindow.onlineList(this->username);
    chatWindow.client->setAddress(this->address);
    chatWindow.client->setPort(this->port);
    chatWindow.client->join();
    chatWindow.show();
    this->close();
}
void Choosecipher::onCaesar(){
    cipher = 0;
}
void Choosecipher::onReverseCaesar(){
    cipher = 1;
}
