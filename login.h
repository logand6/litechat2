#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QTextEdit>
#include <QTcpSocket>
#include "choosecipher.h"

class Login : public QWidget
{
  Q_OBJECT

  public:
    Login(QWidget *parent = 0);


  private slots:
    void onConnect();


  private:
    Choosecipher window2;
    QLabel      *addressLabel;
    QLabel      *portLabel;
    QLabel      *usernameLabel;
    QTextEdit   *addressText;
    QTextEdit   *portText;
    QTextEdit   *usernameText;
    QPushButton *join;
    QTcpSocket  *socket;

    QString     address;
    int         port;
    QString     username;

};

#endif
