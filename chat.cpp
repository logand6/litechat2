#include "chat.h"
#include "client.h"
#include "caesar.h"
#include <QDebug>

Chat::Chat(QWidget *parent) : QWidget(parent)
{
    client   = new Client();
    caesar   = new Caesar(shift);
    chatArea = new QTextEdit(this);
    chatArea->setReadOnly(true);
    chatArea->setText("");
    chatArea->setGeometry(10, 10, 375, 250);

    message = new QTextEdit(this);
    message->setText("");
    message->setGeometry(10, 265, 375, 25);

    sendButton = new QPushButton("Send",this);
    sendButton->setGeometry(398, 263, 113, 37);

    usersOnline = new QLabel("Users Online", this);
    usersOnline->setGeometry(415, 5, 100, 30);

    users = new QTextEdit(this);
    users->setReadOnly(true);
    users->setGeometry(405, 45, 100, 212);

    connect(sendButton, SIGNAL(clicked()), this, SLOT(onSend()));
    connect(client->socket, SIGNAL(readyRead()), this, SLOT(readMessages()));

}
void Chat::setCipher(int cipher){
    this->cipher = cipher;
}
void Chat::setShift(int shift){
    this->shift = shift;
}
void Chat::setAddress(QString address){
    this->address = address;
}
void Chat::setPort(int port){
    this->port = port;
}
void Chat::setUsername(QString username){
    this->username = username;
}

void Chat::onlineList(QString username){
    QList<QString> online;
    online.append(username);
    foreach(QString user, online){
        users->append(user);
    }
}
void Chat::readMessages(){

        if(cipher == 0){
            while(client->socket->canReadLine()){
                QString line = client->socket->readLine().trimmed();
                qDebug() << "Message back from server: " + line;
                caesar->decrypt(line);

                chatArea->append(line);
                qDebug() << "Message back from server after decrpyt: " + line;
            }
        }
        else{
            while(client->socket->canReadLine()){
                QString line = client->socket->readLine().trimmed();
                qDebug() << "Message back from server: " + line;
                caesar->encrypt(line);

                chatArea->append(line);
                qDebug() << "Message back from server after decrpyt: " + line;
            }
        }


}

void Chat::onSend()
{
    if(message->toPlainText()== ""){
        return;
    }
    else{
        QString m = username + ": " + message->toPlainText();
        // caesar cipher
        if(cipher == 0){
            caesar->encrypt(m);
            client->socket->write((m + "\n").toUtf8());

            message->clear();
            message->setFocus();
        }
        // reverse cipher
        else{
            caesar->decrypt(m);
            client->socket->write((m + "\n").toUtf8());

            message->clear();
            message->setFocus();
        }

    }
}




