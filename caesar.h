#ifndef CAESAR_H
#define CAESAR_H
#include <QString>
#include <QList>


class Caesar
{
public:
    Caesar(int key);
    ~Caesar();
    QString encrypt(QString &text);
    QString decrypt(QString & text);
private:
    int shift;
};

#endif // CAESAR_H
