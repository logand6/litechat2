#ifndef CLIENT_H
#define CLIENT_H
#include <QTcpSocket>

class Client
{
public:
    Client();
    ~Client();
    QTcpSocket *socket;

    void join();
    void setAddress(QString address);
    void setPort(int port);

private:
    QString ip;
    int port;
};

#endif // CLIENT_H
