#include "login.h"
#include "choosecipher.h"

Login::Login(QWidget *parent)
    : QWidget(parent)
{

  window2.setFixedSize(500, 215);
  window2.setWindowTitle("Chat");

  addressLabel = new QLabel("IP Address", this);
  addressLabel->setGeometry(50, 40, 75, 30);

  addressText = new QTextEdit(this);
  addressText->setText("localhost");
  addressText->setGeometry(150, 40, 200, 30);

  portLabel = new QLabel("Port #", this);
  portLabel->setGeometry(50, 100, 75, 30);

  portText = new QTextEdit(this);
  portText->setText("6789");
  portText->setGeometry(150, 100, 200, 30);

  usernameLabel = new QLabel("Username", this);
  usernameLabel->setGeometry(50, 160, 75, 30);

  usernameText = new QTextEdit(this);
  usernameText->setText("Anonymous");
  usernameText->setGeometry(150, 160, 200, 30);

  join = new QPushButton("Connect", this);
  join->setGeometry(200, 225, 100, 30);


  connect(join, SIGNAL(clicked()), this, SLOT(onConnect()));
}

void Login::onConnect()
{
    window2.setAddress(addressText->toPlainText());
    window2.setPort(portText->toPlainText().toInt());
    window2.setUsername(usernameText->toPlainText());
    this->close();
    window2.show();

}
