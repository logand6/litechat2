#include "login.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  Login window;

  window.setFixedSize(500, 300);
  window.setWindowTitle("Chat");
  window.show();

  return app.exec();
}
