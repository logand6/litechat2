#include "caesar.h"

Caesar::Caesar(int key)
{
    shift = key;
}

QString
Caesar::encrypt(QString &text){
    QList<QChar> alphabet({'a','b','c','d','e','f','g','h','i','j','k','l','m',
                          'n','o','p','q','r','s','t','u','v','w','x','y','z',' '});

    QList<QChar> upper({'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                       'O','P','Q','R','S','T','U','V','W','X','Y','Z', ' '});


    for(int i = 0; i < text.size(); i++)
        {
            for(int j = 0; j < 27; j++)
            {
                if(text.at(i) == alphabet.at(j)) // lower-case
                {
                    int shiftby = (j+shift)%27;
                    text.replace(i, 1, alphabet.at(shiftby));
                    break;
                }
                else if(text.at(i) == upper.at(j)) // upper-case
                {
                    int shiftby = (j+shift)%27;
                    text.replace(i, 1, upper.at(shiftby));
                    break;
                }
            }
        }
    return text;
}

QString
Caesar::decrypt(QString &text){
    QList<QChar> alphabet({'a','b','c','d','e','f','g','h','i','j','k','l','m',
                          'n','o','p','q','r','s','t','u','v','w','x','y','z',' '});

    QList<QChar> upper({'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                       'O','P','Q','R','S','T','U','V','W','X','Y','Z', ' '});

    for(int i=0; i < text.size(); i++)
        {
            for(int j=0; j < 27; j++)
            {
                if(text.at(i) == alphabet.at(j)) // lower-case
                {
                    int shiftby = (j-shift + 27)%27;
                    text.replace(i, 1, alphabet.at(shiftby));
                    break;
                }
                else if(text.at(i) == upper.at(j)) //upper-case
                {
                    int shiftby = (j-shift + 27)%27;
                    text.replace(i, 1, upper.at(shiftby));
                    break;
                }
            }
        }
    return text;
}

Caesar::~Caesar()
{

}

