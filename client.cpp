#include "client.h"

Client::Client(){
    this->socket = new QTcpSocket;
}

void Client::join(){

    this->socket->connectToHost(ip,port);
}

void Client::setAddress(QString address){
    this->ip = address;
}
void Client::setPort(int port){
    this->port = port;
}

Client::~Client()
{

}
